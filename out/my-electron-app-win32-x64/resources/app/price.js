let randomNumber = Math.floor(Math.random() * 50) + 1;
const guesses = document.querySelector(".guesses");
const lastResult = document.querySelector(".lastResult");
const lowOrHi = document.querySelector(".lowOrHi");
const guessSubmit = document.querySelector(".guessSubmit");
const guessField = document.querySelector(".guessField");
const appendReset = document.getElementById("appendReset");
let guessCount = 1;
let resetButton;

function checkGuess() {
  const userGuess = Number(guessField.value);
  if (guessCount === 1) {
    guesses.textContent = "Choix précédents: ";
  }

  if ((userGuess <= 50) & (userGuess >= 0)) {
    guesses.textContent += userGuess + " ";

    if (userGuess === randomNumber) {
      lastResult.textContent = "Bravo! Vous avez trouvé!";
      lastResult.style.backgroundColor = "green";
      lowOrHi.textContent = "";
      setGameOver();
    } else if (guessCount === 5) {
      lastResult.textContent = "! GAME OVER !";
      lowOrHi.textContent = "";
      setGameOver();
    } else {
      lastResult.textContent = "Perdu!";
      lastResult.style.backgroundColor = "red";
      if (userGuess < randomNumber) {
        lowOrHi.textContent = "Trop bas!";
      } else if (userGuess > randomNumber) {
        lowOrHi.textContent = "Trop haut!";
      }
    }

    guessCount++;
    guessField.value = "";
    guessField.focus();
  } else {
    alert("Vous devez choisir un chiffre entre 0 et 50");
  }
}
document.addEventListener("DOMContentLoaded", function () {
  guessSubmit.addEventListener("click", checkGuess);
});

function setGameOver() {
  guessField.disabled = true;
  guessSubmit.disabled = true;
  resetButton = document.createElement("button");
  resetButton.textContent = "Rejouer";
  resetButton.classList.add("btn", "btn-primary");
  appendReset.appendChild(resetButton);
  resetButton.addEventListener("click", resetGame);
}

function resetGame() {
  guessCount = 1;
  const resetParas = document.querySelectorAll(".resultParas p");
  for (const resetPara of resetParas) {
    resetPara.textContent = "";
  }

  resetButton.parentNode.removeChild(resetButton);
  guessField.disabled = false;
  guessSubmit.disabled = false;
  guessField.value = "";
  guessField.focus();
  lastResult.style.backgroundColor = "white";
  randomNumber = Math.floor(Math.random() * 50) + 1;
}

module.exports = randomNumber;
