const guessCount = require('../sum');

test('Nombre de coups joués', () => {
    expect(guessCount(3)).toBeLessThanOrEqual(5);
});