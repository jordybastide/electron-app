const randomNumber = require("../price");
describe("randomNumber", () => {
  test("test 1", () => {
    //inputs
    const result = randomNumber;
    //expect
    expect(result).toBeLessThan(51);
    expect(result).toBeGreaterThanOrEqual(0);
  });
});
