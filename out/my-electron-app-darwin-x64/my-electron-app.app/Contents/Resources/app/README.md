# electron-app

## about this project

We are using Node.js 16.13.2, Chromium 100.0.4896.143, and Electron 18.2.0.

If you have nvm installed :

`nvm use 16.13.2`

Else, you will need to install node 16.13.2 manually

https://nodejs.org/en/download/

This project is a game app where the user has to guess the right price. (as in the famous tv show)

The app has dark/light mode feature and notifications. (they were'nt requested but were easy to add following electron's documentations)

## installation instructions

`npm install`

`npm start`

## binaries

Binaries (win, macOS, linux) can be found in /out folder or downloadable at deploy stage (CI)

To generate binaries :

`npm install --save-dev @electron-forge/cli`

`npx electron-forge import`

`npm run make`


## tests

`npm run test`

Will run every *.test.js files (inside ./__tests__)

## gitlab ci

To run gitlab ci, go to CI/CD, Jobs and run it manually

You will be able to download binaries after the deploy final step